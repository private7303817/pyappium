# _*_coding:utf-8 _*_
# @Time　　:2021/6/21 22:31
# @Author　 : king
# @File　　  :base_driver.py
# @Software  :PyCharm
from appium import webdriver
from common.all_path import configPath, appPath
import yaml
import os
import json

from common.app_info import get_devices_version, get_app_name, get_app_launchable_activity, get_app_package_name

from common.appium_auto_server import open_appium


class BaseDriver:

    def __init__(self, device_info):
        self.device_info = device_info
        cmd = "appium -a 127.0.0.1 -p {0}".format(self.device_info["server_port"])
        print("---------------执行BaseDriver初始化命令:" + cmd)
        open_appium(cmd, self.device_info["server_port"])
        print("---------------执行BaseDriver初始化命令-执行成功")

    def base_driver(self, automationName="UiAutomator2"):
        fp = open(f"{configPath}//caps.yml", encoding='utf-8')
        # 平台名称、包名、Activity名称、超时时间、是否重置、server_ip、
        desired_caps = yaml.load(fp, Loader=yaml.FullLoader)

        # 设备名称
        desired_caps["deviceName"] = self.device_info['device']
        # 设备名称(ios)
        desired_caps["udid"] = self.device_info['device']
        # 版本信息
        desired_caps["platform_version"] = get_devices_version(desired_caps["deviceName"])

        app_path = os.path.join(appPath, get_app_name(appPath))
        desired_caps['app'] = app_path

        desired_caps['appPackage'] = get_app_package_name()

        desired_caps['appActivity'] = get_app_launchable_activity()

        # 系统端口号
        desired_caps["systemPort"] = self.device_info["system_port"]
        print("--------desired_caps的值为:" + json.dumps(desired_caps))
        if automationName != "UiAutomator2":
            desired_caps["automationName"] = automationName
        try:
            driver = webdriver.Remote(f"http://127.0.0.1:{self.device_info['server_port']}",
                                      desired_capabilities=desired_caps)
            print("driver初始化成功……")

            return driver
        except Exception as e:
            print('driver初始化失败，重新初始化！！！')
