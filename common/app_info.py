# _*_coding:utf-8 _*_
# @Time　　:2021/6/19 22:45
# @Author　 : king
# @File　　  :app_info.py
# @Software  :PyCharm
import logging
import os
import re
import subprocess
import time

from common.all_path import appPath


def exec_cmd(cmd) -> str:
    result = os.popen(cmd).read()
    logging.info(str(result))
    return result


def get_uid():
    # 构建 adb shell 命令字符串
    adb_cmd = "adb shell dumpsys package " + get_app_package_name()
    print("-----------adb_cmd是" + adb_cmd)

    try:
        # 执行命令获取应用程序的信息
        result = exec_cmd(adb_cmd)
        print("-----------result" + result)
        # 使用正则表达式从输出中匹配 UID
        uid_match = re.search(r'uid=(\d+)', result)
        if uid_match:
            uid = uid_match.group(1)
            return uid
        else:
            return None

    except subprocess.CalledProcessError as e:
        print("Error executing command:", e)
        return None


def get_app_name(file_dir) -> str:
    for root, dirs, files in os.walk(file_dir):
        print("-------------查找apk")
        files = [file for file in files if file.endswith(".apk")]

        if len(files) == 1:
            print("-------------找到apk")
            return files[0]
        else:
            raise FileNotFoundError("{}目录下没有测试包或者存在多个测试包。。".format(file_dir))


def get_app_package_name() -> str:
    print("------------获得apk名字")
    cmd = "%ANDROID_HOME%\\build-tools\\29.0.3\\aapt dump badging {} | findstr package".format(
        os.path.join(appPath, get_app_name(appPath)))
    result = exec_cmd(cmd)

    print("------------执行完aapt dump")
    if "package" in result:
        package_name = result.strip().split(" ")[1].split('=')[1].replace("'", '')
        print("------------包名为" + package_name)
        return package_name
    else:
        raise NameError("未获取到package name。")


def get_app_launchable_activity() -> str:
    cmd = "%ANDROID_HOME%\\build-tools\\29.0.3\\aapt dump badging {} | findstr launchable".format(
        os.path.join(appPath, get_app_name(appPath)))
    print("将要执行查找apk文件" + cmd)
    result = exec_cmd(cmd)
    print("------" + result)
    if "launchable" in result:
        launchable_activity = result.strip().split(" ")[1].split('=')[1].replace("label", '').replace("'", '')
        print("---------活动名为" + launchable_activity)
        return launchable_activity
    else:
        raise NameError("未获取到launchable activity。")


def get_devices_version(device: str) -> str:
    if not isinstance(device, str):
        raise Exception("device type is should str..")
    result = exec_cmd("adb -s {} shell getprop ro.build.version.release".format(device))
    result = result.strip()
    if "error" not in result:
        return result
    else:
        raise Exception("获取设备系统版本失败，无法进行正常测试。。")


def get_all_devices() -> list:
    result = exec_cmd('adb devices')
    result = result.strip().split(" ")[3].replace("\n", '').replace("\t", ''). \
        replace("attached", '').split('device')
    result.remove('')
    if len(result) == 0:
        raise Exception("电脑未连接设备信息，无法进行正常测试。。")

    return result


def get_device_infos():
    device_infos = []
    devices = get_all_devices()
    for i in range(len(devices)):
        device_dict = {"platform_version": get_devices_version(devices[i]), "server_port": 4723 + i * 2,
                       "system_port": 8200 + i * 1, "device": devices[i]}
        device_infos.append(device_dict)

    if len(device_infos) < 1:
        raise Exception("当前电脑未连接设备信息。。。")

    return device_infos


def install_app(device_list: str) -> None:
    if not isinstance(device_list, list):
        raise Exception("device_list is should list!")

    for device_info in device_list:
        cmd = 'adb -s {} install "{}"'.format(device_info.get("device").split(':')[-1],
                                              str(os.path.join(appPath, get_app_name(appPath))))

        print("------------准备执行:" + cmd)
        logging.info("开始安装应用：{}".format(cmd))
        # exec_cmd(cmd) 用这个就不行，不知道为什么
        os.popen(cmd)

        print("---------等待出现继续安装按钮")
        # 等待安装完成
        time.sleep(10)  # 等待 5 秒，确保应用已安装完成

        # 点击继续按钮
        print("-----------------准备点击继续安装按钮...")
        cmd_tap = 'adb -s {} shell input tap 304 2044'.format(
            device_info.get("device").split(':')[-1])  # 替换 x 和 y 为继续按钮的坐标
        subprocess.run(cmd_tap, shell=True)
        print("----------------完成安装")

        time.sleep(3)
        cmd1 = "adb shell pm grant com.xkw.client android.permission.ACCESS_FINE_LOCATION"
        cmd2 = "adb shell pm grant com.xkw.client android.permission.ACCESS_COARSE_LOCATION"
        cmd3 = "adb shell pm grant com.xkw.client android.permission.ACCESS_BACKGROUND_LOCATION"

        os.system(cmd1)
        os.system(cmd2)
        os.system(cmd3)
        print("-------开启位置权限")


def uninstall_app(device_list: list) -> None:
    if not isinstance(device_list, list):
        raise Exception("device_list is should list!")

    for device_info in device_list:
        cmd = 'adb -s {} uninstall "{}"'.format(device_info.get("device").split(':')[-1],
                                                str(get_app_package_name())).replace("'", '')
        print("------------准备执行:" + cmd)
        logging.info("开始卸载设备上应用：{}".format(cmd))
        exec_cmd(cmd)
        print("------------执行adb -s结束")
