# _*_coding:utf-8 _*_
# @Time　　:2021/6/21 22:33
# @Author　 : king
# @File　　  :main.py
# @Software  :PyCharm
from multiprocessing import Pool

import os
import pytest

from common.all_path import appPath
from common.app_info import get_device_infos, uninstall_app, install_app
from common.appium_auto_server import close_appium


def run_parallel(device_info):
    pytest.main([f"--cmdopt={device_info}",
                 "--alluredir", "outputs/reports/data"])
    os.system("allure generate outputs/reports/data -o outputs/reports/html --clean")


if __name__ == "__main__":
    device_lists = get_device_infos()
    print(device_lists[0])
    uninstall_app(device_lists)
    print("----------已卸载手机上的app")
    install_app(device_lists)
    print("----------已重新安装app")
    with Pool(len(device_lists)) as pool:
        print("----------进入程序")
        pool.map(run_parallel, device_lists)
        pool.close()
        pool.join()

    print("关闭appium")
    close_appium()

